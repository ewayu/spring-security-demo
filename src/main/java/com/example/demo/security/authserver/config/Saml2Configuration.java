package com.example.demo.security.authserver.config;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.example.demo.security.authserver.jwk.Jwks;
import com.nimbusds.jose.jwk.RSAKey;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.saml2.core.Saml2X509Credential;
import org.springframework.security.saml2.provider.service.registration.InMemoryRelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistration;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrations;
import org.springframework.security.web.SecurityFilterChain;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

@EnableWebSecurity
//@Configuration
public class Saml2Configuration {

    @Bean
    SecurityFilterChain saml2SecurityFilterChain(HttpSecurity http) throws Exception {
        http.saml2Login(conf -> {});
        return http.build();
    }

    @Bean
    RelyingPartyRegistrationRepository relyingPartyRegistrationRepository(AmazonS3 amazonS3) throws Exception {
        GetObjectRequest request = new GetObjectRequest("bucket", "name");
        final S3Object jksObject = amazonS3.getObject(request);
        final KeyStore keyStore = Jwks.load(jksObject.getObjectContent(), "keystorepwd" .toCharArray());
        // basing on "keytool -genkeypair -keyalg RSA -alias rsm -keystore rsm_saml_keystore.jks -storepass rsmpass -keypass rsmpass"
        final RSAKey rsakey = RSAKey.load(keyStore, "rsm", "keypwd" .toCharArray());
        final PrivateKey key = rsakey.toPrivateKey();

        // basing on "keytool -importcert -alias adfs -keystore ehm_saml_keystore.jks -file idp.cert"
        final X509Certificate certificate = (X509Certificate) keyStore.getCertificate("adfs");

        Saml2X509Credential credentials = Saml2X509Credential.signing(key, certificate);
        RelyingPartyRegistration registration = RelyingPartyRegistrations
                .fromMetadataLocation("https://ap.example.org/metadata")
                .registrationId("id")
                .singleLogoutServiceLocation("{baseUrl}/logout/saml2/slo")
                .signingX509Credentials(signing -> signing.add(credentials))
                .build();
        return new InMemoryRelyingPartyRegistrationRepository(registration);
    }
}
