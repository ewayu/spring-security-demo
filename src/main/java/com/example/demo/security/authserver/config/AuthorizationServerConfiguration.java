package com.example.demo.security.authserver.config;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.example.demo.security.authserver.authentication.OAuth2ResourceOwnerPasswordAuthenticationConverter;
import com.example.demo.security.authserver.authentication.OAuth2ResourceOwnerPasswordAuthenticationProvider;
import com.example.demo.security.authserver.jwk.Jwks;
import com.example.demo.security.authserver.userdetails.EmsUserDetailsService;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.OAuth2Token;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.server.authorization.InMemoryOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2TokenType;
import org.springframework.security.oauth2.server.authorization.client.InMemoryRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configurers.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.oauth2.server.authorization.settings.AuthorizationServerSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;
import org.springframework.security.oauth2.server.authorization.web.authentication.*;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.security.KeyStore;
import java.time.Duration;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

@EnableConfigurationProperties({Jwks.KeyStoreConfigProperties.class})
@Configuration
@RequiredArgsConstructor
public class AuthorizationServerConfiguration {

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    SecurityFilterChain authorizationServerSecurityFilterChain(HttpSecurity http) throws Exception {
        final OAuth2AuthorizationServerConfigurer authorizationServerConfigurer = oAuth2AuthorizationServerConfigurer();

        RequestMatcher endpointsMatcher = authorizationServerConfigurer.getEndpointsMatcher();

        http
                .securityMatcher(endpointsMatcher)
                .authorizeHttpRequests(authorizeRequests -> authorizeRequests.anyRequest().authenticated())
                .csrf(csrf -> csrf.ignoringRequestMatchers(endpointsMatcher))
                .httpBasic(Customizer.withDefaults())
                .apply(authorizationServerConfigurer);

        final DefaultSecurityFilterChain filterChain = http.build();

        addCustomOAuth2ResourceOwnerPasswordAuthenticationProvider(http);

        return filterChain;
    }

    @Bean
    OAuth2AuthorizationServerConfigurer oAuth2AuthorizationServerConfigurer() {
        OAuth2AuthorizationServerConfigurer authorizationServerConfigurer = new OAuth2AuthorizationServerConfigurer();
        authorizationServerConfigurer.tokenEndpoint((tokenEndpoint) -> tokenEndpoint.accessTokenRequestConverter(
                new DelegatingAuthenticationConverter(Arrays.asList(
                        new OAuth2AuthorizationCodeAuthenticationConverter(),
                        new OAuth2RefreshTokenAuthenticationConverter(),
                        new OAuth2ClientCredentialsAuthenticationConverter(),
                        // customize OAuth2TokenEndpointFilter to add 'Resource Owner Password' grant_type as
                        // Spring authorization server removed support for it due to OAuth 2.1 draft
                        new OAuth2ResourceOwnerPasswordAuthenticationConverter()))
        ));
        return authorizationServerConfigurer;
    }

    @Bean
    OAuth2AuthorizationService authorizationService(RegisteredClientRepository registeredClientRepository) {
        return new InMemoryOAuth2AuthorizationService();
    }


    @SuppressWarnings("unchecked")
    private void addCustomOAuth2ResourceOwnerPasswordAuthenticationProvider(HttpSecurity http) {

        AuthenticationManager authenticationManager = http.getSharedObject(AuthenticationManager.class);
        OAuth2AuthorizationService authorizationService = http.getSharedObject(OAuth2AuthorizationService.class);
        OAuth2TokenGenerator<? extends OAuth2Token> tokenGenerator = http.getSharedObject(OAuth2TokenGenerator.class);

        OAuth2ResourceOwnerPasswordAuthenticationProvider resourceOwnerPasswordAuthenticationProvider =
                new OAuth2ResourceOwnerPasswordAuthenticationProvider(authenticationManager, authorizationService, tokenGenerator);

        // This will add new authentication provider in the list of existing authentication providers.
        http.authenticationProvider(resourceOwnerPasswordAuthenticationProvider);
    }

    @Bean
    RegisteredClientRepository registeredClientRepository(PasswordEncoder passwordEncoder) {
        final RegisteredClient client = RegisteredClient.withId(UUID.randomUUID().toString())
                .clientId("hub_next_user")
                .clientSecret(passwordEncoder.encode("A1b2c3d4"))
                .clientAuthenticationMethods(s -> {
//                    s.add(ClientAuthenticationMethod.NONE);
                    s.add(ClientAuthenticationMethod.CLIENT_SECRET_POST);
//                    s.add(ClientAuthenticationMethod.CLIENT_SECRET_BASIC);
                })
//                .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
                .authorizationGrantType(AuthorizationGrantType.PASSWORD)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
//                .redirectUri("http://127.0.0.1:8080/404")
                .scope("trust")
//                .clientSettings(ClientSettings.builder()
//                        .requireAuthorizationConsent(true)
//                        .requireProofKey(false)
//                        .build())
                .tokenSettings(TokenSettings.builder()
                        .accessTokenFormat(OAuth2TokenFormat.SELF_CONTAINED)
                        .idTokenSignatureAlgorithm(SignatureAlgorithm.RS256)
                        .accessTokenTimeToLive(Duration.ofSeconds(30 * 60))
                        .refreshTokenTimeToLive(Duration.ofSeconds(60 * 60))
                        .reuseRefreshTokens(true)
                        .build())
                .build();
        return new InMemoryRegisteredClientRepository(client);
    }

    // load existing JWK from s3 bucket (for Kernel app)
//    @Bean
    @SneakyThrows
    JWKSource<SecurityContext> exitingJwkSource(AmazonS3 amazonS3, Jwks.KeyStoreConfigProperties keyStoreConf) {
        GetObjectRequest request = new GetObjectRequest(keyStoreConf.bucketname(), keyStoreConf.objectname());
        final S3Object jwkObject = amazonS3.getObject(request);
        final KeyStore keyStore = Jwks.load(jwkObject.getObjectContent(), keyStoreConf.password().toCharArray());
        final JWKSet jwkSet = JWKSet.load(keyStore, name -> keyStoreConf.keyKairPassword().toCharArray());
        return (jwkSelector, securityContext) -> jwkSelector.select(jwkSet);
    }


    // generate new JWK to sign JWT (for AIO app)
    @Bean
    JWKSource<SecurityContext> newJwkSource() {
        final JWKSet jwkSet = Jwks.create();
        return (jwkSelector, securityContext) -> jwkSelector.select(jwkSet);
    }

//    @Bean
    OAuth2TokenCustomizer<JwtEncodingContext> tokenCustomizer() {
        return (context) -> {
            if (OAuth2TokenType.ACCESS_TOKEN.equals(context.getTokenType())) {
                context.getClaims().claims(claim -> {
                    claim.put("role", context.getPrincipal().getAuthorities().stream()
                            .map(GrantedAuthority::getAuthority).collect(Collectors.toSet()));
                });
            }
        };
    }

//    @Bean
    AuthorizationServerSettings authorizationServerSettings(@Value("${oauth2.token.issuer}") String tokenIssuer) {
        return AuthorizationServerSettings.builder()
                .issuer(tokenIssuer)
                .build();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    UserDetailsService users() {
        return new EmsUserDetailsService();
    }

}
