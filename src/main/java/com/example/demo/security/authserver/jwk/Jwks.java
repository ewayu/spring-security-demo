package com.example.demo.security.authserver.jwk;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import lombok.SneakyThrows;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.InputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.UUID;

public final class Jwks {

    @SneakyThrows
    public static KeyStore load(InputStream resource, char[] password) {
        try (InputStream closable = resource) {
            // EMS uses jks type's key store
            KeyStore keyStore = KeyStore.getInstance("jks");
            keyStore.load(closable, password);
            return keyStore;
        }
    }

    public static JWKSet create() {
        RSAKey rsaKey = generateRsa();
        return new JWKSet(rsaKey);
    }

    public static RSAKey generateRsa() {
        KeyPair keyPair = KeyGeneratorUtils.generateRsaKey();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        return new RSAKey.Builder(publicKey)
                .privateKey(privateKey)
                .keyID(UUID.randomUUID().toString())
                .build();
    }

    @ConfigurationProperties(prefix = "oauth2.keystore")
    public record KeyStoreConfigProperties(String bucketname, String objectname, String password, String keyKairPassword) {
    }

    static class KeyGeneratorUtils {

        private KeyGeneratorUtils() {
        }

        static KeyPair generateRsaKey() {
            KeyPair keyPair;
            try {
                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
                keyPairGenerator.initialize(2048);
                keyPair = keyPairGenerator.generateKeyPair();
            } catch (Exception ex) {
                throw new IllegalStateException(ex);
            }
            return keyPair;
        }
    }

}
